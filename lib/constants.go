package lib

// MaxTime Number constants
const (
	MaxTime = 24 * 4
)

// SubQueue Types of sub-queue in the main queue
type SubQueue int

const (
	Resuscitation SubQueue = iota
	Urgent
	Common
)

// MaxCapacity Maximum capacity of different sub-queues
type MaxCapacity int

const (
	ResuscitationQueueCapacity MaxCapacity = 1
	UrgentQueueCapacity        MaxCapacity = 1
	CommonQueueCapacity        MaxCapacity = 50
)

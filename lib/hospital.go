package lib

//
//import (
//	"math/rand"
//	"time"
//)
//
//var Uid int = 0
//
//type Hospital struct {
//	ResuscitationUnit ResuscitationUnit
//	FirstAidUnit      []FirstAidUnit
//	Patients          []*Patient
//}
//
//func (h Hospital) Score() int {
//	score := 0
//	//for _, patient := range h.Patients {
//		//score += patient.Penalty()
//	}
//	return score
//}
//func NewRandomHospital(nbFistAid int, nbResuscitation int, nbPatientResuscitation int, nbPatientFirstAid int) Hospital {
//	patients := make([]*Patient, 0, nbPatientFirstAid+nbPatientResuscitation)
//
//	resuscitationUnit := ResuscitationUnit{
//		NumberOfUnit: nbResuscitation,
//		QueuePatient: generateRandomResuscitationQueue(nbPatientResuscitation),
//	}
//	patients = append(patients, resuscitationUnit.QueuePatient...)
//
//	queueFistAidUnit := generateRandomFirstAidQueue(nbPatientFirstAid, nbFistAid)
//	firstAidUnits := make([]FirstAidUnit, nbFistAid)
//	for i := 0; i < nbFistAid; i++ {
//		firstAidUnits[i] = FirstAidUnit{
//			QueuePatient: queueFistAidUnit[i],
//		}
//		patients = append(patients, queueFistAidUnit[i]...)
//	}
//
//	return Hospital{
//		ResuscitationUnit: resuscitationUnit,
//		FirstAidUnit:      firstAidUnits,
//		Patients:          patients,
//	}
//}
//
//func generateRandomResuscitationQueue(nbPatient int) []*Patient {
//	rand.Seed(time.Now().Unix())
//	patients := make([]*Patient, 0, nbPatient)
//	for i := 0; i < nbPatient/2; i++ {
//		category := IMMEDIATE
//
//		currentPatient := Patient{
//			Start:             i,
//			End:               i + 1,
//			Location:          RESUSCITATION_UNIT,
//			Category:          category,
//			TreatmentDuration: 1,
//			Uid:               Uid,
//		}
//		patients = append(patients, &currentPatient)
//		Uid += 1
//	}
//
//	for i := nbPatient / 2; i < nbPatient; i++ {
//		category := DELAYED
//
//		currentPatient := Patient{
//			Start:             i,
//			End:               i + 1,
//			Location:          RESUSCITATION_UNIT,
//			Category:          category,
//			TreatmentDuration: 1,
//			Uid:               Uid,
//		}
//		patients = append(patients, &currentPatient)
//		Uid += 1
//	}
//
//	return patients
//}
//
//func generateRandomFirstAidQueue(nbPatient int, nbUnit int) [][]*Patient {
//	rand.Seed(time.Now().Unix())
//	patients := make([][]*Patient, nbPatient)
//	for i := 0; i < nbUnit; i++ {
//		patients[i] = make([]*Patient, 0, nbPatient/nbUnit)
//	}
//	for i := 0; i < nbUnit; i++ {
//		patients[i] = append(patients[i], generateFirstAidPatient(0))
//	}
//	for i := nbUnit; i < nbPatient; i++ {
//		lastPatientIndex := len(patients[i%nbUnit]) - 1
//		newPatient := generateFirstAidPatient(patients[i%nbUnit][lastPatientIndex].End)
//		patients[i%nbUnit] = append(patients[i%nbUnit], newPatient)
//	}
//	return patients
//}
//
//func generateFirstAidPatient(start int) *Patient {
//	treatmentDuration := rand.Intn(10)
//	Uid += 1
//	return &Patient{
//		Start:             start,
//		End:               start + treatmentDuration,
//		Location:          FIRST_AID_UNIT,
//		Category:          MINIMAL,
//		TreatmentDuration: treatmentDuration,
//		Uid:               Uid,
//	}
//}

package lib

import (
	"math"
)

// first place patient into hole
// patient waiting the shortest
// moving cost

func LocalSearch(p Patient, q Queue, currentTime int) (sq SubQueue, index int) {
	// Decide optimal place for patient
	optimalPlace := p.OptimalPlaceInQueue()
	optimalQueue := p.OptimalQueue()

	switch {
	case optimalQueue == Resuscitation:
		// Trivial case
		return Resuscitation, optimalPlace
	case optimalQueue == Urgent:
		// Trivial case
		return Urgent, optimalPlace
	default:
		// Main case
		return Common, FindPlaceToInsertInCommonQueue(currentTime, p, q)
	}

}

func FindPlaceToInsertInCommonQueue(currentTime int, p Patient, q Queue) (index int) {
	// Find costs of inserting patient at different deviations from optimal
	searchFrom := p.OptimalPlaceInQueue()
	higherScoreLastPatient := q.IndexOfLastPatientWithHigherScore(p)
	queueLength := q.CommonQueueLength()

	if searchFrom < higherScoreLastPatient {
		searchFrom = higherScoreLastPatient
	}

	index = math.MaxInt32
	previousCost := math.MaxInt32
	for i := searchFrom; i < queueLength; i++ {
		cost := q.CalculateCostToInsertNewPatient(p, currentTime, i)
		if previousCost > cost {
			previousCost = cost
			index = i
		} else {
			// Breaks the loop after local minimum
			break
		}
	}
	return index
}

// urgent place first

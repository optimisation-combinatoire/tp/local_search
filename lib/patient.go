package lib

import "fmt"

type Patient struct {
	IdentificationNumber int
	SeverityScore        int
	ArrivalTime          int
	Category             int
	TreatmentDuration    int
	QueueCost            int
}

func (p Patient) String() string {
	return fmt.Sprintf(
		"Id:- %v, "+
			"Score:- %v, "+
			"Arrival:- %v, "+
			"QueueCost:- %v | ",
		p.IdentificationNumber,
		p.SeverityScore,
		p.ArrivalTime,
		p.QueueCost)
}

// CalculateQueueCost calculates the cost of waiting in the queue by multiplying
// severity score to the time patient has been in the queue
func (p Patient) CalculateQueueCost(currentTime int) int {
	if currentTime < p.ArrivalTime {
		currentTime = p.ArrivalTime + 1
	}
	return p.SeverityScore * (currentTime - p.ArrivalTime)
}

// CostToMovePatientFromCurrentPlace calculate the cost to move by specific
// periods
func (p Patient) CostToMovePatientFromCurrentPlace(moveByPeriods int) int {
	return p.QueueCost * moveByPeriods
}

// OptimalPlaceInQueue place in the queue if all queue is empty
func (p Patient) OptimalPlaceInQueue() int {
	waitTime := 0
	switch {
	case p.SeverityScore > 800:
		waitTime = 0
	case p.SeverityScore > 600:
		waitTime = 0
	case p.SeverityScore > 400:
		waitTime = 3
	case p.SeverityScore > 200:
		waitTime = 5
	default:
		waitTime = 7
	}
	return waitTime
}

// OptimalQueue place in the queue to put patient
func (p Patient) OptimalQueue() SubQueue {
	switch {
	case p.SeverityScore > 800:
		return Resuscitation
	case p.SeverityScore > 600:
		return Urgent
	default:
		return Common
	}
}

func (p Patient) HowLongPatientIsWaiting(currentTime int) int {
	return currentTime - p.ArrivalTime
}

package lib

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Queue struct {
	// Contains three sub-queues
	Resuscitation []Patient
	Urgent        []Patient
	Common        []Patient
}

func (q Queue) String() string {
	return fmt.Sprintf(
		"Resuscitation:- %v\n"+
			"Urgent:-        %v \n"+
			"Common:-        %v",
		q.Resuscitation,
		q.Urgent,
		q.Common)
}

// CalculateQueueCost que cost for each patient is calculated and stored in Patient.QueueCost
func (q Queue) CalculateQueueCost(currentTime int) {
	for i, patient := range q.Resuscitation {
		q.Resuscitation[i].QueueCost = patient.CalculateQueueCost(currentTime)
	}
	for i, patient := range q.Urgent {
		q.Urgent[i].QueueCost = patient.CalculateQueueCost(currentTime)
	}
	for i, patient := range q.Common {
		q.Common[i].QueueCost = patient.CalculateQueueCost(currentTime)
	}
}

// CalculateCostToInsertNewPatient total queue cost of insertion at times away from optimal
func (q Queue) CalculateCostToInsertNewPatient(p Patient, currentTime int, searchFrom int) int {
	awayFromOptimalBy := searchFrom - p.OptimalPlaceInQueue()

	q.CalculateQueueCost(currentTime)

	// Each patient queue cost in the queue
	patientCost := p.SeverityScore * awayFromOptimalBy // New patient queue cost at this time

	// Cost due to shift of queue
	// Shift queue from the optimal time + timesAwayFromOptimal
	queueCost := 0
	for i, patient := range q.Common {
		if i > searchFrom {
			queueCost += patient.SeverityScore
		}
	}
	if patientCost > queueCost {
		return patientCost
	} else {
		return queueCost
	}
}

// CreateQueueFromJsonFile creates queue from given json file
func CreateQueueFromJsonFile(f string) Queue {
	// Create initial queueJson
	// Open jsonFile
	queueJson, err := os.Open(f)
	if err != nil {
		fmt.Println(err)
	}

	queue := new(Queue)
	jsonBites, _ := ioutil.ReadAll(queueJson)
	err = json.Unmarshal(jsonBites, &queue)
	if err != nil {
		fmt.Println("error:", err)
	}

	// defer the closing of jsonFile
	defer func(queue *os.File) {
		err := queue.Close()
		if err != nil {
			fmt.Println("Closing error")
		}
	}(queueJson)

	return *queue
}

// InsertInQueue inserts patient at insertAt position in the sub-queue
func (q Queue) InsertInQueue(p Patient, subQueue SubQueue, insertAt int) Queue {
	// If insertAt is beyond last place return queue
	if !q.BoundsCorrect(subQueue, insertAt) {
		return q
	}
	// Insert patient in the queue
	var queue = make([]Patient, len(q.Common)+1)
	switch {
	case subQueue == Resuscitation:
		firstPart := q.Resuscitation[:insertAt]
		lastPart := q.Resuscitation[insertAt:]
		copy(queue, firstPart)
		queue = append(queue[:insertAt], p)
		queue = append(queue[:insertAt+1], lastPart...)
		q.Resuscitation = queue
	case subQueue == Urgent:
		firstPart := q.Urgent[:insertAt]
		lastPart := q.Urgent[insertAt:]
		copy(queue, firstPart)
		queue = append(queue[:insertAt], p)
		queue = append(queue[:insertAt+1], lastPart...)
		q.Urgent = queue
	case subQueue == Common:
		firstPart := q.Common[:insertAt]
		lastPart := q.Common[insertAt:]
		copy(queue, firstPart)
		queue = append(queue[:insertAt], p)
		queue = append(queue[:insertAt+1], lastPart...)
		q.Common = queue
	}
	return q
}

// BoundsCorrect checks the bounds of given sub-queue
func (q Queue) BoundsCorrect(subQueue SubQueue, insertAt int) bool {
	switch {
	case subQueue == Resuscitation:
		return len(q.Resuscitation) >= insertAt
	case subQueue == Urgent:
		return len(q.Urgent) >= insertAt
	case subQueue == Common:
		return len(q.Common) >= insertAt
	default:
		return false
	}
}

// PopPatient removes the last element of sub-queue and returns
func (q *Queue) PopPatient(subQueue SubQueue) Patient {
	var patient Patient
	switch {
	case subQueue == Resuscitation:
		patient = q.Resuscitation[len(q.Resuscitation)-1]
		q.Resuscitation = q.Resuscitation[:len(q.Resuscitation)-1]
		return patient
	case subQueue == Urgent:
		patient = q.Urgent[len(q.Urgent)-1]
		q.Urgent = q.Urgent[:len(q.Urgent)-1]
		return patient
	case subQueue == Common:
		patient = q.Common[len(q.Common)-1]
		q.Common = q.Common[:len(q.Common)-1]
		return patient
	default:
		return patient
	}
}

// ResuscitationQueueLength returns number of patients in the queue
func (q Queue) ResuscitationQueueLength() MaxCapacity {
	return MaxCapacity(len(q.Resuscitation))
}

// IsResuscitationQueueFull return true if que is full
func (q Queue) IsResuscitationQueueFull() bool {
	return q.ResuscitationQueueLength() >= ResuscitationQueueCapacity
}

// UrgentQueueLength returns number of patients in the queue
func (q Queue) UrgentQueueLength() MaxCapacity {
	return MaxCapacity(len(q.Urgent))
}

// IsUrgentQueueFull return true if que is full
func (q Queue) IsUrgentQueueFull() bool {
	return q.UrgentQueueLength() >= UrgentQueueCapacity
}

// CommonQueueLength returns number of patients in the queue
func (q Queue) CommonQueueLength() int {
	return len(q.Common)
}

// IsCommonQueueFull return true if que is full
func (q Queue) IsCommonQueueFull() bool {
	return q.CommonQueueLength() >= int(CommonQueueCapacity)
}

func (q Queue) IndexOfLastPatientWithHigherScore(p Patient) (indexOfHighestScore int) {
	for i, patient := range q.Common {
		if p.SeverityScore <= patient.SeverityScore {
			indexOfHighestScore = i
		}
	}
	return indexOfHighestScore
}

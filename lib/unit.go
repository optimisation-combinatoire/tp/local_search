package lib

type Unit interface {
	InsertAndUpdatePatient(int, *Patient)
	Queue() []*Patient
}

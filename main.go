package main

import (
	"fmt"

	"gitlab.com/optimisation-combinatoire/tp/local_search/lib"
)

func main() {

	fmt.Println("\n\n\nDisplay queue")
	// Create initial queueJson
	// Open jsonFile
	q := lib.CreateQueueFromJsonFile("./data/initial_queue_16_long.json")

	p := lib.Patient{
		IdentificationNumber: 1,
		SeverityScore:        1,
		ArrivalTime:          1,
		Category:             1,
		TreatmentDuration:    1,
	}
	currentTime := 15

	subQueue, index := lib.LocalSearch(p, q, currentTime)
	fmt.Printf("Insert this patient in queue:- %v at index:- %v\n", subQueue, index)

	//Todo: Try to club together patient, which will have similar treatment duration
}

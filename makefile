GOCMD = go
BUILD_PATH = ./build
BINARY = $(BUILD_PATH)/data_generator 
TEST_PATH = ./test/...

.PHONY: build test

build:
	mkdir -p build
	$(GOCMD) build -o $(BINARY) main.go
test:
	$(GOCMD) test -v ${TEST_PATH}

run: build
	$(BINARY)


package test

//
//import (
//	"fmt"
//	"log"
//	"reflect"
//	"testing"
//
//	"../lib"
//)
//
//func TestInsertFistAid(t *testing.T) {
//	queue := []*lib.Patient{
//		{Uid: 0, Start: 0, End: 3, TreatmentDuration: 3},
//		{Uid: 1, Start: 3, End: 4, TreatmentDuration: 1},
//		{Uid: 2, Start: 4, End: 8, TreatmentDuration: 4},
//		{Uid: 3, Start: 8, End: 10, TreatmentDuration: 2},
//	}
//	unit := lib.FirstAidUnit{
//		QueuePatient: queue,
//	}
//	unit.InsertAndUpdatePatient(1, &lib.Patient{Uid: -1, TreatmentDuration: 3})
//	expectedQueue := []*lib.Patient{
//		{Uid: 0, Start: 0, End: 3, TreatmentDuration: 3},
//		{Uid: -1, Start: 3, End: 6, TreatmentDuration: 3, Location: lib.FIRST_AID_UNIT},
//		{Uid: 1, Start: 6, End: 7, TreatmentDuration: 1},
//		{Uid: 2, Start: 7, End: 11, TreatmentDuration: 4},
//		{Uid: 3, Start: 11, End: 13, TreatmentDuration: 2},
//	}
//	if !reflect.DeepEqual(expectedQueue, unit.QueuePatient) {
//		log.Println("got")
//		got := ""
//		for _, el := range unit.QueuePatient {
//			got += fmt.Sprintf("%v ", el)
//		}
//		log.Println("expected")
//		expected := ""
//		for _, el := range expectedQueue {
//			expected += fmt.Sprintf("%v ", el)
//		}
//		t.Fatalf("The slice doesn't match\n expected: %v\ngot: %v", expected, got)
//	}
//}
//
//func TestInsertAtTheBeginningFistAid(t *testing.T) {
//	queue := []*lib.Patient{
//		{Uid: 0, Start: 0, End: 3, TreatmentDuration: 3},
//		{Uid: 1, Start: 3, End: 4, TreatmentDuration: 1},
//		{Uid: 2, Start: 4, End: 8, TreatmentDuration: 4},
//		{Uid: 3, Start: 8, End: 10, TreatmentDuration: 2},
//	}
//	unit := lib.FirstAidUnit{
//		QueuePatient: queue,
//	}
//	unit.InsertAndUpdatePatient(0, &lib.Patient{Uid: -1, TreatmentDuration: 3})
//	expectedQueue := []*lib.Patient{
//		{Uid: -1, Start: 0, End: 3, TreatmentDuration: 3, Location: lib.FIRST_AID_UNIT},
//		{Uid: 0, Start: 3, End: 6, TreatmentDuration: 3},
//		{Uid: 1, Start: 6, End: 7, TreatmentDuration: 1},
//		{Uid: 2, Start: 7, End: 11, TreatmentDuration: 4},
//		{Uid: 3, Start: 11, End: 13, TreatmentDuration: 2},
//	}
//	if !reflect.DeepEqual(expectedQueue, unit.QueuePatient) {
//		log.Println("got")
//		got := ""
//		for _, el := range unit.QueuePatient {
//			got += fmt.Sprintf("%v ", el)
//		}
//		log.Println("expected")
//		expected := ""
//		for _, el := range expectedQueue {
//			expected += fmt.Sprintf("%v ", el)
//		}
//		t.Fatalf("The slice doesn't match\n expected: %v\ngot: %v", expected, got)
//	}
//}
//
//func TestInsertAtTheEndFistAid(t *testing.T) {
//	queue := []*lib.Patient{
//		{Uid: 0, Start: 0, End: 3, TreatmentDuration: 3},
//		{Uid: 1, Start: 3, End: 4, TreatmentDuration: 1},
//		{Uid: 2, Start: 4, End: 8, TreatmentDuration: 4},
//		{Uid: 3, Start: 8, End: 10, TreatmentDuration: 2},
//	}
//	unit := lib.FirstAidUnit{
//		QueuePatient: queue,
//	}
//	unit.InsertAndUpdatePatient(4, &lib.Patient{Uid: -1, TreatmentDuration: 3})
//	expectedQueue := []*lib.Patient{
//		{Uid: 0, Start: 0, End: 3, TreatmentDuration: 3},
//		{Uid: 1, Start: 3, End: 4, TreatmentDuration: 1},
//		{Uid: 2, Start: 4, End: 8, TreatmentDuration: 4},
//		{Uid: 3, Start: 8, End: 10, TreatmentDuration: 2},
//		{Uid: -1, Start: 10, End: 13, TreatmentDuration: 3, Location: lib.FIRST_AID_UNIT},
//	}
//	if !reflect.DeepEqual(expectedQueue, unit.QueuePatient) {
//		log.Println("got")
//		got := ""
//		for _, el := range unit.QueuePatient {
//			got += fmt.Sprintf("%v ", el)
//		}
//		log.Println("expected")
//		expected := ""
//		for _, el := range expectedQueue {
//			expected += fmt.Sprintf("%v ", el)
//		}
//		t.Fatalf("The slice doesn't match\n expected: %v\ngot: %v", expected, got)
//	}
//}

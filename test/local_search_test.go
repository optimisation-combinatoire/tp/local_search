package test

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/optimisation-combinatoire/tp/local_search/lib"
)

func TestLocalSearchTrivialCase(t *testing.T) {
	p := lib.Patient{
		IdentificationNumber: 1,
		SeverityScore:        1000,
		ArrivalTime:          15,
		Category:             1,
		TreatmentDuration:    1,
	}
	q := lib.CreateQueueFromJsonFile("../data/initial_queue_16_long.json")
	currentTime := 15

	subQueue, index := lib.LocalSearch(p, q, currentTime)

	if index != 0 {
		t.Errorf("Not correct indexl")
	}

	if subQueue != lib.Resuscitation {
		t.Errorf("Not correct indexl")
	}
}

func TestFindOptimalPlaceCommonQueue(t *testing.T) {
	p := lib.Patient{
		IdentificationNumber: 16,
		SeverityScore:        450,
		ArrivalTime:          15,
		Category:             1,
		TreatmentDuration:    1,
	}
	q := lib.CreateQueueFromJsonFile("../data/initial_queue_16_long.json")
	currentTime := 15

	index := lib.FindPlaceToInsertInCommonQueue(currentTime, p, q)
	if index != 8 {
		t.Errorf("Not correct index")
	}
}

func TestLocalSearch(t *testing.T) {
	startTime := time.Now()
	p := lib.Patient{
		IdentificationNumber: 16,
		SeverityScore:        450,
		ArrivalTime:          15,
		Category:             1,
		TreatmentDuration:    1,
	}
	q := lib.CreateQueueFromJsonFile("../data/initial_queue_16_long.json")
	currentTime := 15

	subQueue, index := lib.LocalSearch(p, q, currentTime)

	if subQueue != lib.Common {
		t.Errorf("Not correct queue")
	}

	if index != 8 {
		t.Errorf("Not correct index to insert")
	}
	endTime := time.Now()
	fmt.Println("Short queue 16 patient, execution time:- ", endTime.Sub(startTime))
}

func TestLocalSearchLongQueue(t *testing.T) {
	startTime := time.Now()
	p := lib.Patient{
		IdentificationNumber: 26,
		SeverityScore:        300,
		ArrivalTime:          35,
		Category:             1,
		TreatmentDuration:    1,
	}
	q := lib.CreateQueueFromJsonFile("../data/initial_queue_25_long.json")
	currentTime := 35

	subQueue, index := lib.LocalSearch(p, q, currentTime)

	if subQueue != lib.Common {
		t.Errorf("Not correct queue")
	}

	if index != 14 {
		t.Errorf("Not correct index to insert")
	}
	endTime := time.Now()
	fmt.Println("Long queue 25 patient, execution time:- ", endTime.Sub(startTime))
}

func TestLocalSearchLongLongQueue(t *testing.T) {
	startTime := time.Now()
	p := lib.Patient{
		IdentificationNumber: 56,
		SeverityScore:        300,
		ArrivalTime:          55,
		Category:             1,
		TreatmentDuration:    1,
	}
	q := lib.CreateQueueFromJsonFile("../data/initial_queue_50_long.json")
	currentTime := 55

	subQueue, index := lib.LocalSearch(p, q, currentTime)

	if subQueue != lib.Common {
		t.Errorf("Not correct queue")
	}

	if index != 26 {
		t.Errorf("Not correct index to insert")
	}
	endTime := time.Now()
	fmt.Println("Long queue 50 patient, execution time:- ", endTime.Sub(startTime))
}

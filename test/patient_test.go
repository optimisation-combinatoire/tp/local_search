package test

import (
	"testing"

	"gitlab.com/optimisation-combinatoire/tp/local_search/lib"
)

func TestCalculateQueueCost(t *testing.T) {
	p := lib.Patient{
		IdentificationNumber: 1,
		SeverityScore:        1,
		ArrivalTime:          1,
		Category:             1,
		TreatmentDuration:    1,
	}
	currentTime := 45
	queueCost := p.CalculateQueueCost(currentTime)
	expectedCost := p.SeverityScore * (currentTime - p.ArrivalTime)

	if queueCost != expectedCost {
		t.Errorf("QueCosts are not equal")
	}
}

func TestCostToMovePatientFromCurrentPlace(t *testing.T) {
	p := lib.Patient{
		IdentificationNumber: 1,
		SeverityScore:        1,
		ArrivalTime:          1,
		Category:             1,
		TreatmentDuration:    1,
		QueueCost:            40,
	}
	moveByPeriods := 2
	expectedCost := p.QueueCost * moveByPeriods

	if expectedCost != p.CostToMovePatientFromCurrentPlace(moveByPeriods) {
		t.Errorf("Cost to move are not equal are not equal")
	}
}

func TestOptimalWaitTime(t *testing.T) {
	p := lib.Patient{
		IdentificationNumber: 1,
		SeverityScore:        450,
		ArrivalTime:          1,
		Category:             1,
		TreatmentDuration:    1,
		QueueCost:            40,
	}
	expectedOptimalWaitTime := 3

	if expectedOptimalWaitTime != p.OptimalPlaceInQueue() {
		t.Errorf("Optimal wait time is not correct")
	}
}

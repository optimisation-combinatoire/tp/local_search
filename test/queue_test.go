package test

import (
	"../lib"
	"testing"
)

func TestIndexOfLastPatientWithHigherScore(t *testing.T) {
	p := lib.Patient{
		IdentificationNumber: 1,
		SeverityScore:        445,
		ArrivalTime:          1,
		Category:             1,
		TreatmentDuration:    1,
	}
	q := lib.CreateQueueFromJsonFile("../data/initial_queue_16_long.json")
	expectedIndex := 5

	index := q.IndexOfLastPatientWithHigherScore(p)
	if index != expectedIndex {
		t.Errorf("Not good index")
	}
}

package test

//
//import (
//	"reflect"
//	"testing"
//
//	"../lib"
//)
//
//func TestInsertRessucitationUnit(t *testing.T) {
//	queue := []*lib.Patient{
//		{Uid: 0, Start: 0, End: 1},
//		{Uid: 1, Start: 0, End: 1},
//		{Uid: 2, Start: 0, End: 1},
//	}
//	unit := lib.ResuscitationUnit{
//		QueuePatient: queue,
//		NumberOfUnit: 3,
//	}
//	unit.InsertAndUpdatePatient(1, &lib.Patient{Uid: -1})
//	expectedQueue := []*lib.Patient{
//		{Uid: 0, Start: 0, End: 1},
//		{Uid: -1, Start: 0, End: 1, Location: lib.RESUSCITATION_UNIT},
//		{Uid: 1, Start: 0, End: 1},
//		{Uid: 2, Start: 1, End: 2},
//	}
//	if !reflect.DeepEqual(expectedQueue, unit.QueuePatient) {
//		t.Fatalf("The slice doesn't match\n expected: %v\ngot: %v", expectedQueue, unit.QueuePatient)
//	}
//}
//
//func TestInsertAtThebeginingRessucitationUnit(t *testing.T) {
//	queue := []*lib.Patient{
//		{Uid: 0, Start: 0, End: 1},
//		{Uid: 1, Start: 0, End: 1},
//		{Uid: 2, Start: 0, End: 1},
//	}
//	unit := lib.ResuscitationUnit{
//		QueuePatient: queue,
//		NumberOfUnit: 3,
//	}
//	unit.InsertAndUpdatePatient(0, &lib.Patient{Uid: -1})
//	expectedQueue := []*lib.Patient{
//		{Uid: -1, Start: 0, End: 1, Location: lib.RESUSCITATION_UNIT},
//		{Uid: 0, Start: 0, End: 1},
//		{Uid: 1, Start: 0, End: 1},
//		{Uid: 2, Start: 1, End: 2},
//	}
//	if !reflect.DeepEqual(expectedQueue, unit.QueuePatient) {
//		t.Fatalf("The slice doesn't match\n expected: %v\ngot: %v", expectedQueue, unit.QueuePatient)
//	}
//}
//
//func TestInsertAtTheEndRessucitationUnit(t *testing.T) {
//	queue := []*lib.Patient{
//		{Uid: 0, Start: 0, End: 1},
//		{Uid: 1, Start: 0, End: 1},
//		{Uid: 2, Start: 0, End: 1},
//	}
//	unit := lib.ResuscitationUnit{
//		QueuePatient: queue,
//		NumberOfUnit: 3,
//	}
//	unit.InsertAndUpdatePatient(2, &lib.Patient{Uid: -1})
//	expectedQueue := []*lib.Patient{
//		{Uid: 0, Start: 0, End: 1},
//		{Uid: 1, Start: 0, End: 1},
//		{Uid: 2, Start: 0, End: 1},
//		{Uid: -1, Start: 1, End: 2, Location: lib.RESUSCITATION_UNIT},
//	}
//	if !reflect.DeepEqual(expectedQueue, unit.QueuePatient) {
//		t.Fatalf("The slice doesn't match\n expected: %v\ngot: %v", expectedQueue, unit.QueuePatient)
//	}
//}
